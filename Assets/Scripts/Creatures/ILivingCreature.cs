namespace Gameplay
{
    namespace Creatures
    {
        public interface ILivingCreature
        {
            public int MaxHP { get; set; }
            public int HP { get; set; }
        }
    }
}


